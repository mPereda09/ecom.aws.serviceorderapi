﻿using ECOM.AWS.ServiceOrderApi.Entities;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Text;

namespace ECOM.AWS.ServiceOrderApi.Services
{
    public class ServiceOrder
    {

        public StatusResponse GetServiceOrder(string OrderId, Dictionary<string, string> StageVariables)
        {
            StatusResponse orderStatus = new StatusResponse();
            try
            {
                WebRequest request = WebRequest.Create(StageVariables["UrlOrderStatus"] + OrderId);
                request.Method = "GET";
                request.Headers.Add("Accept", "application/json");
                request.Headers.Add("Content-Type", "application/json");
                request.Headers.Add("X-VTEX-API-AppKey", StageVariables["Authorization"]);
                request.Headers.Add("X-VTEX-API-AppToken", StageVariables["OrderStatusApiToken"]);

                WebResponse responseClient = request.GetResponse();
                using (var dataStream = responseClient.GetResponseStream())
                {
                    StreamReader reader = new StreamReader(dataStream);
                    string responseFromServer = reader.ReadToEnd();
                    var OrderResults = JObject.Parse(responseFromServer);

                    if (OrderResults != null)
                    {
                        var resulStatus = OrderResults.GetValue("status");
                        var resulOrderId = OrderResults.GetValue("orderId");
                        orderStatus = GetOrderStatus(resulStatus.ToString(), resulOrderId.ToString());
                    }
                    else
                    {
                        return new StatusResponse()
                        {
                            Code = 637121959265504511,
                            Message = "La orden no Existe",
                            Result = new Result()
                            {
                                OrderId = OrderId,
                                OrderStatus = null,
                                OrderReceiveStatus = null
                            }
                        };
                    }

                }
                responseClient.Close();
            }
            catch (WebException webException)
            {
                WebExceptionStatus webExceptionStatus = webException.Status;
                if (webExceptionStatus == WebExceptionStatus.ProtocolError)
                {
                    HttpWebResponse HttpWebResponse = (HttpWebResponse)webException.Response;
                    var aSCIIEncoding = ASCIIEncoding.ASCII;
                    using (var reader = new System.IO.StreamReader(HttpWebResponse.GetResponseStream(), aSCIIEncoding))
                    {
                        return new StatusResponse()
                        {
                            Code = 637121961334425437,
                            Message = webException.Message.ToString(),
                            Result = null
                        };
                    }
                }
                else
                {
                    return new StatusResponse()
                    {
                        Code = 637121961334425437,
                        Message = webException.Message.ToString(),
                        Result = null
                    };
                }

            }
            catch (Exception ex)
            {
                return new StatusResponse()
                {
                    Code = 637121961334425437,
                    Message = ex.Message.ToString(),
                    Result = null
                };

            }
            return orderStatus;
        }

        private StatusResponse GetOrderStatus(string orderIdResult, string orderStatusResult)
        {
            StatusResponse objreturn = new StatusResponse();
            if (!string.IsNullOrEmpty(orderIdResult) && !string.IsNullOrEmpty(orderStatusResult))
            {
                objreturn.Code = 0;
                objreturn.Message = null;
                objreturn.Result = new Result();
                objreturn.Result.OrderId = orderStatusResult;
                objreturn.Result.OrderReceiveStatus = orderIdResult;
            }
            else
            {
                objreturn = null;
            }

            return objreturn;
        }
    }
}
