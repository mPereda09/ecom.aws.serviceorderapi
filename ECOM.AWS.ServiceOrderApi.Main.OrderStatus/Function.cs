using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using Amazon.Lambda.APIGatewayEvents;
using Amazon.Lambda.Core;
using ECOM.AWS.ServiceOrderApi.Bussines;
using ECOM.AWS.ServiceOrderApi.Core;
using ECOM.AWS.ServiceOrderApi.Entities;
using Microsoft.AspNetCore.Http;
using Newtonsoft.Json;

// Assembly attribute to enable the Lambda function's JSON input to be converted into a .NET class.
[assembly: LambdaSerializer(typeof(Amazon.Lambda.Serialization.Json.JsonSerializer))]

namespace ECOM.AWS.ServiceOrderApi.Main.OrderStatus
{
    public class Function
    {
        /// <summary>
        /// A simple function that takes a string and does a ToUpper
        /// </summary>
        /// <param name="input"></param>
        /// <param name="context"></param>
        /// <returns></returns>
        public APIGatewayProxyResponse GetStatusOrder(APIGatewayProxyRequest request, ILambdaContext context)
        {
            APIGatewayProxyResponse response = new APIGatewayProxyResponse();
            string infoError = string.Empty;
            infoError = "ECOM.AWS.ServiceOrderApi.Main.OrderStatus.Function.GetStatusOrder()";
            try
            {

                Dictionary<string, string> pathParams = request.PathParameters != null ? new Dictionary<string, string>(request.PathParameters, StringComparer.OrdinalIgnoreCase) : null;
                Dictionary<string, string> stageVariables = request.StageVariables != null ? new Dictionary<string, string>(request.StageVariables, StringComparer.OrdinalIgnoreCase) : null;

                StatusOrderBz objbz = new StatusOrderBz();
                Dictionary<string, string> Queryparameters = request.QueryStringParameters != null ? new Dictionary<string, string>(request.QueryStringParameters, StringComparer.OrdinalIgnoreCase) : null;
                Dictionary<string, string> StageVariables = request.StageVariables != null ? new Dictionary<string, string>(request.StageVariables, StringComparer.OrdinalIgnoreCase) : null;
                StaticVariables.SetStageVariables(StageVariables);
                StatusResponse objResult = objbz.GetOrderStatus(pathParams, StageVariables);
                response.StatusCode = (int)HttpStatusCode.OK;
                response.Body = JsonConvert.SerializeObject(objResult);                
            }
            catch (Exception ex)
            {
                StatusResponse StatusResponse = new StatusResponse()
                {
                    Code = 637121961334425437,
                    Message = "Ocurrio error al invocar Servicio : " + infoError + "\\" + "MoreInfo : " + ex.Message,
                    Result = null
                };
            }

            return response;
        }
    }
}
