﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace ECOM.AWS.ServiceOrderApi.Entities
{
    public class OrderResponse
    {
        [JsonProperty("orderId")]
        public string OrderId { get; set; }

        [JsonProperty("sequence")]

        public long Sequence { get; set; }

        [JsonProperty("marketplaceOrderId")]
        public string MarketplaceOrderId { get; set; }

        [JsonProperty("marketplaceServicesEndpoint")]
        public Uri MarketplaceServicesEndpoint { get; set; }

        [JsonProperty("sellerOrderId")]
        public object SellerOrderId { get; set; }

        [JsonProperty("origin")]
        public string Origin { get; set; }

        [JsonProperty("affiliateId")]
        public string AffiliateId { get; set; }

        [JsonProperty("salesChannel")]
        public long SalesChannel { get; set; }

        [JsonProperty("merchantName")]
        public object MerchantName { get; set; }

        [JsonProperty("status")]
        public string Status { get; set; }

        [JsonProperty("statusDescription")]
        public string StatusDescription { get; set; }

        [JsonProperty("value")]
        public long Value { get; set; }

        [JsonProperty("creationDate")]
        public DateTimeOffset CreationDate { get; set; }

        [JsonProperty("lastChange")]
        public DateTimeOffset LastChange { get; set; }

        [JsonProperty("orderGroup")]
        public object OrderGroup { get; set; }

        [JsonProperty("totals")]
        public List<Total> Totals { get; set; }

        [JsonProperty("items")]
        public List<ItemElement> Items { get; set; }

        [JsonProperty("marketplaceItems")]
        public List<object> MarketplaceItems { get; set; }

        [JsonProperty("clientProfileData")]
        public ClientProfileData ClientProfileData { get; set; }

        [JsonProperty("giftRegistryData")]
        public object GiftRegistryData { get; set; }

        [JsonProperty("marketingData")]
        public object MarketingData { get; set; }

        [JsonProperty("ratesAndBenefitsData")]
        public RatesAndBenefitsData RatesAndBenefitsData { get; set; }

        [JsonProperty("shippingData")]
        public ShippingData ShippingData { get; set; }

        [JsonProperty("paymentData")]
        public PaymentData PaymentData { get; set; }

        [JsonProperty("packageAttachment")]
        public PackageAttachment PackageAttachment { get; set; }

        [JsonProperty("sellers")]
        public List<Seller> Sellers { get; set; }

        [JsonProperty("callCenterOperatorData")]
        public object CallCenterOperatorData { get; set; }

        [JsonProperty("followUpEmail")]
        public string FollowUpEmail { get; set; }

        [JsonProperty("lastMessage")]
        public object LastMessage { get; set; }

        [JsonProperty("hostname")]
        public string Hostname { get; set; }

        [JsonProperty("invoiceData")]
        public object InvoiceData { get; set; }

        [JsonProperty("changesAttachment")]
        public object ChangesAttachment { get; set; }

        [JsonProperty("openTextField")]
        public OpenTextField OpenTextField { get; set; }

        [JsonProperty("roundingError")]
        public long RoundingError { get; set; }

        [JsonProperty("orderFormId")]
        public object OrderFormId { get; set; }

        [JsonProperty("commercialConditionData")]
        public object CommercialConditionData { get; set; }

        [JsonProperty("isCompleted")]
        public bool IsCompleted { get; set; }

        [JsonProperty("customData")]
        public object CustomData { get; set; }

        [JsonProperty("storePreferencesData")]
        public StorePreferencesData StorePreferencesData { get; set; }

        [JsonProperty("allowCancellation")]
        public bool AllowCancellation { get; set; }

        [JsonProperty("allowEdition")]
        public bool AllowEdition { get; set; }

        [JsonProperty("isCheckedIn")]
        public bool IsCheckedIn { get; set; }

        [JsonProperty("marketplace")]
        public Marketplace Marketplace { get; set; }

        [JsonProperty("authorizedDate")]
        public DateTimeOffset AuthorizedDate { get; set; }

        [JsonProperty("invoicedDate")]
        public object InvoicedDate { get; set; }

        [JsonProperty("cancelReason")]
        public object CancelReason { get; set; }

        [JsonProperty("itemMetadata")]
        public ItemMetadata ItemMetadata { get; set; }

        [JsonProperty("subscriptionData")]
        public object SubscriptionData { get; set; }
    }

    public class ClientProfileData
    {
        [JsonProperty("id")]
        public string Id { get; set; }

        [JsonProperty("email")]
        public string Email { get; set; }

        [JsonProperty("firstName")]
        public string FirstName { get; set; }

        [JsonProperty("lastName")]
        public string LastName { get; set; }

        [JsonProperty("documentType")]
        public object DocumentType { get; set; }

        [JsonProperty("document")]
        public object Document { get; set; }

        [JsonProperty("phone")]
        public string Phone { get; set; }

        [JsonProperty("corporateName")]
        public object CorporateName { get; set; }

        [JsonProperty("tradeName")]
        public object TradeName { get; set; }

        [JsonProperty("corporateDocument")]
        public object CorporateDocument { get; set; }

        [JsonProperty("stateInscription")]
        public object StateInscription { get; set; }

        [JsonProperty("corporatePhone")]
        public object CorporatePhone { get; set; }

        [JsonProperty("isCorporate")]
        public bool IsCorporate { get; set; }

        [JsonProperty("userProfileId")]
        public object UserProfileId { get; set; }

        [JsonProperty("customerClass")]
        public object CustomerClass { get; set; }
    }

    public class ItemMetadata
    {
        [JsonProperty("Items")]
        public List<Item> Items { get; set; }
    }

    public class Item
    {
        [JsonProperty("Id")]

        public long Id { get; set; }

        [JsonProperty("Seller")]

        public long Seller { get; set; }

        [JsonProperty("Name")]
        public string Name { get; set; }

        [JsonProperty("SkuName")]
        public string SkuName { get; set; }

        [JsonProperty("ProductId")]

        public long ProductId { get; set; }

        [JsonProperty("RefId")]

        public long RefId { get; set; }

        [JsonProperty("Ean")]

        public long Ean { get; set; }

        [JsonProperty("ImageUrl")]
        public Uri ImageUrl { get; set; }

        [JsonProperty("DetailUrl")]
        public string DetailUrl { get; set; }

        [JsonProperty("AssemblyOptions")]
        public List<object> AssemblyOptions { get; set; }
    }

    public class ItemElement
    {
        [JsonProperty("uniqueId")]
        public string UniqueId { get; set; }

        [JsonProperty("id")]

        public long Id { get; set; }

        [JsonProperty("productId")]

        public long ProductId { get; set; }

        [JsonProperty("ean")]

        public long Ean { get; set; }

        [JsonProperty("lockId")]
        public string LockId { get; set; }

        [JsonProperty("itemAttachment")]
        public ItemAttachment ItemAttachment { get; set; }

        [JsonProperty("attachments")]
        public List<object> Attachments { get; set; }

        [JsonProperty("quantity")]
        public long Quantity { get; set; }

        [JsonProperty("seller")]

        public long Seller { get; set; }

        [JsonProperty("name")]
        public string Name { get; set; }

        [JsonProperty("refId")]

        public long RefId { get; set; }

        [JsonProperty("price")]
        public long Price { get; set; }

        [JsonProperty("listPrice")]
        public long ListPrice { get; set; }

        [JsonProperty("manualPrice")]
        public object ManualPrice { get; set; }

        [JsonProperty("priceTags")]
        public List<object> PriceTags { get; set; }

        [JsonProperty("imageUrl")]
        public Uri ImageUrl { get; set; }

        [JsonProperty("detailUrl")]
        public object DetailUrl { get; set; }

        [JsonProperty("components")]
        public List<object> Components { get; set; }

        [JsonProperty("bundleItems")]
        public List<object> BundleItems { get; set; }

        [JsonProperty("params")]
        public List<object> Params { get; set; }

        [JsonProperty("offerings")]
        public List<object> Offerings { get; set; }

        [JsonProperty("sellerSku")]

        public long SellerSku { get; set; }

        [JsonProperty("priceValidUntil")]
        public object PriceValidUntil { get; set; }

        [JsonProperty("commission")]
        public long Commission { get; set; }

        [JsonProperty("tax")]
        public long Tax { get; set; }

        [JsonProperty("preSaleDate")]
        public object PreSaleDate { get; set; }

        [JsonProperty("additionalInfo")]
        public AdditionalInfo AdditionalInfo { get; set; }

        [JsonProperty("measurementUnit")]
        public string MeasurementUnit { get; set; }

        [JsonProperty("unitMultiplier")]
        public long UnitMultiplier { get; set; }

        [JsonProperty("sellingPrice")]
        public long SellingPrice { get; set; }

        [JsonProperty("isGift")]
        public bool IsGift { get; set; }

        [JsonProperty("shippingPrice")]
        public object ShippingPrice { get; set; }

        [JsonProperty("rewardValue")]
        public long RewardValue { get; set; }

        [JsonProperty("freightCommission")]
        public long FreightCommission { get; set; }

        [JsonProperty("priceDefinitions")]
        public object PriceDefinitions { get; set; }

        [JsonProperty("taxCode")]
        public string TaxCode { get; set; }

        [JsonProperty("parentItemIndex")]
        public object ParentItemIndex { get; set; }

        [JsonProperty("parentAssemblyBinding")]
        public object ParentAssemblyBinding { get; set; }

        [JsonProperty("callCenterOperator")]
        public object CallCenterOperator { get; set; }

        [JsonProperty("serialNumbers")]
        public object SerialNumbers { get; set; }

        [JsonProperty("costPrice")]
        public long CostPrice { get; set; }
    }

    public class AdditionalInfo
    {
        [JsonProperty("brandName")]
        public string BrandName { get; set; }

        [JsonProperty("brandId")]

        public long BrandId { get; set; }

        [JsonProperty("categoriesIds")]
        public string CategoriesIds { get; set; }

        [JsonProperty("categories")]
        public List<Category> Categories { get; set; }

        [JsonProperty("productClusterId")]
        public string ProductClusterId { get; set; }

        [JsonProperty("commercialConditionId")]

        public long CommercialConditionId { get; set; }

        [JsonProperty("dimension")]
        public Dimension Dimension { get; set; }

        [JsonProperty("offeringInfo")]
        public object OfferingInfo { get; set; }

        [JsonProperty("offeringType")]
        public object OfferingType { get; set; }

        [JsonProperty("offeringTypeId")]
        public object OfferingTypeId { get; set; }
    }

    public class Category
    {
        [JsonProperty("id")]
        public long Id { get; set; }

        [JsonProperty("name")]
        public string Name { get; set; }
    }

    public class Dimension
    {
        [JsonProperty("cubicweight")]
        public double Cubicweight { get; set; }

        [JsonProperty("height")]
        public long Height { get; set; }

        [JsonProperty("length")]
        public long Length { get; set; }

        [JsonProperty("weight")]
        public long Weight { get; set; }

        [JsonProperty("width")]
        public long Width { get; set; }
    }

    public class ItemAttachment
    {
        [JsonProperty("content")]
        public Con Content { get; set; }

        [JsonProperty("name")]
        public object Name { get; set; }
    }

    public class Con
    {
    }

    public class Marketplace
    {
        [JsonProperty("baseURL")]
        public Uri BaseUrl { get; set; }

        [JsonProperty("isCertified")]
        public bool IsCertified { get; set; }

        [JsonProperty("name")]
        public object Name { get; set; }
    }

    public class OpenTextField
    {
        [JsonProperty("value")]
        public string Value { get; set; }
    }

    public class PackageAttachment
    {
        [JsonProperty("packages")]
        public List<object> Packages { get; set; }
    }

    public class PaymentData
    {
        [JsonProperty("giftCards")]
        public List<object> GiftCards { get; set; }

        [JsonProperty("transactions")]
        public List<Transaction> Transactions { get; set; }
    }

    public class Transaction
    {
        [JsonProperty("isActive")]
        public bool IsActive { get; set; }

        [JsonProperty("transactionId")]
        public object TransactionId { get; set; }

        [JsonProperty("merchantName")]
        public object MerchantName { get; set; }

        [JsonProperty("payments")]
        public List<Payment> Payments { get; set; }
    }

    public class Payment
    {
        [JsonProperty("id")]
        public object Id { get; set; }

        [JsonProperty("paymentSystem")]

        public long PaymentSystem { get; set; }

        [JsonProperty("paymentSystemName")]
        public string PaymentSystemName { get; set; }

        [JsonProperty("value")]
        public long Value { get; set; }

        [JsonProperty("installments")]
        public long Installments { get; set; }

        [JsonProperty("referenceValue")]
        public long ReferenceValue { get; set; }

        [JsonProperty("cardHolder")]
        public object CardHolder { get; set; }

        [JsonProperty("cardNumber")]
        public object CardNumber { get; set; }

        [JsonProperty("firstDigits")]
        public object FirstDigits { get; set; }

        [JsonProperty("lastDigits")]
        public object LastDigits { get; set; }

        [JsonProperty("cvv2")]
        public object Cvv2 { get; set; }

        [JsonProperty("expireMonth")]
        public object ExpireMonth { get; set; }

        [JsonProperty("expireYear")]
        public object ExpireYear { get; set; }

        [JsonProperty("url")]
        public object Url { get; set; }

        [JsonProperty("giftCardId")]
        public object GiftCardId { get; set; }

        [JsonProperty("giftCardName")]
        public object GiftCardName { get; set; }

        [JsonProperty("giftCardCaption")]
        public object GiftCardCaption { get; set; }

        [JsonProperty("redemptionCode")]
        public object RedemptionCode { get; set; }

        [JsonProperty("group")]
        public object Group { get; set; }

        [JsonProperty("tid")]
        public object Tid { get; set; }

        [JsonProperty("dueDate")]
        public object DueDate { get; set; }

        [JsonProperty("connectorResponses")]
        public Con ConnectorResponses { get; set; }
    }

    public class RatesAndBenefitsData
    {
        [JsonProperty("id")]
        public string Id { get; set; }

        [JsonProperty("rateAndBenefitsIdentifiers")]
        public List<object> RateAndBenefitsIdentifiers { get; set; }
    }

    public class Seller
    {
        [JsonProperty("id")]

        public long Id { get; set; }

        [JsonProperty("name")]
        public string Name { get; set; }

        [JsonProperty("logo")]
        public object Logo { get; set; }
    }

    public class ShippingData
    {
        [JsonProperty("id")]
        public string Id { get; set; }

        [JsonProperty("address")]
        public Address Address { get; set; }

        [JsonProperty("logisticsInfo")]
        public List<LogisticsInfo> LogisticsInfo { get; set; }

        [JsonProperty("trackingHints")]
        public List<object> TrackingHints { get; set; }

        [JsonProperty("selectedAddresses")]
        public List<Address> SelectedAddresses { get; set; }
    }

    public class Address
    {
        [JsonProperty("addressType")]
        public string AddressType { get; set; }

        [JsonProperty("receiverName")]
        public string ReceiverName { get; set; }

        [JsonProperty("addressId")]
        public string AddressId { get; set; }

        [JsonProperty("postalCode")]

        public long PostalCode { get; set; }

        [JsonProperty("city")]
        public string City { get; set; }

        [JsonProperty("state")]
        public string State { get; set; }

        [JsonProperty("country")]
        public string Country { get; set; }

        [JsonProperty("street")]
        public string Street { get; set; }

        [JsonProperty("number")]

        public long Number { get; set; }

        [JsonProperty("neighborhood")]
        public string Neighborhood { get; set; }

        [JsonProperty("complement")]
        public string Complement { get; set; }

        [JsonProperty("reference")]
        public string Reference { get; set; }

        [JsonProperty("geoCoordinates")]
        public List<object> GeoCoordinates { get; set; }
    }

    public class LogisticsInfo
    {
        [JsonProperty("itemIndex")]
        public long ItemIndex { get; set; }

        [JsonProperty("selectedSla")]
        public string SelectedSla { get; set; }

        [JsonProperty("lockTTL")]
        public string LockTtl { get; set; }

        [JsonProperty("price")]
        public long Price { get; set; }

        [JsonProperty("listPrice")]
        public long ListPrice { get; set; }

        [JsonProperty("sellingPrice")]
        public long SellingPrice { get; set; }

        [JsonProperty("deliveryWindow")]
        public object DeliveryWindow { get; set; }

        [JsonProperty("deliveryCompany")]
        public string DeliveryCompany { get; set; }

        [JsonProperty("shippingEstimate")]
        public string ShippingEstimate { get; set; }

        [JsonProperty("shippingEstimateDate")]
        public DateTimeOffset ShippingEstimateDate { get; set; }

        [JsonProperty("slas")]
        public object Slas { get; set; }

        [JsonProperty("shipsTo")]
        public object ShipsTo { get; set; }

        [JsonProperty("deliveryIds")]
        public List<DeliveryId> DeliveryIds { get; set; }

        [JsonProperty("deliveryChannel")]
        public string DeliveryChannel { get; set; }

        [JsonProperty("pickupStoreInfo")]
        public PickupStoreInfo PickupStoreInfo { get; set; }

        [JsonProperty("addressId")]
        public string AddressId { get; set; }

        [JsonProperty("polygonName")]
        public object PolygonName { get; set; }
    }

    public class DeliveryId
    {
        [JsonProperty("courierId")]
        public string CourierId { get; set; }

        [JsonProperty("courierName")]
        public string CourierName { get; set; }

        [JsonProperty("dockId")]

        public long DockId { get; set; }

        [JsonProperty("quantity")]
        public long Quantity { get; set; }

        [JsonProperty("warehouseId")]

        public long WarehouseId { get; set; }
    }

    public class PickupStoreInfo
    {
        [JsonProperty("additionalInfo")]
        public object AdditionalInfo { get; set; }

        [JsonProperty("address")]
        public object Address { get; set; }

        [JsonProperty("dockId")]
        public object DockId { get; set; }

        [JsonProperty("friendlyName")]
        public object FriendlyName { get; set; }

        [JsonProperty("isPickupStore")]
        public bool IsPickupStore { get; set; }
    }

    public class StorePreferencesData
    {
        [JsonProperty("countryCode")]
        public string CountryCode { get; set; }

        [JsonProperty("currencyCode")]
        public string CurrencyCode { get; set; }

        [JsonProperty("currencyFormatInfo")]
        public CurrencyFormatInfo CurrencyFormatInfo { get; set; }

        [JsonProperty("currencyLocale")]
        public long CurrencyLocale { get; set; }

        [JsonProperty("currencySymbol")]
        public string CurrencySymbol { get; set; }

        [JsonProperty("timeZone")]
        public string TimeZone { get; set; }
    }

    public class CurrencyFormatInfo
    {
        [JsonProperty("CurrencyDecimalDigits")]
        public long CurrencyDecimalDigits { get; set; }

        [JsonProperty("CurrencyDecimalSeparator")]
        public string CurrencyDecimalSeparator { get; set; }

        [JsonProperty("CurrencyGroupSeparator")]
        public string CurrencyGroupSeparator { get; set; }

        [JsonProperty("CurrencyGroupSize")]
        public long CurrencyGroupSize { get; set; }

        [JsonProperty("StartsWithCurrencySymbol")]
        public bool StartsWithCurrencySymbol { get; set; }
    }

    public class Total
    {
        [JsonProperty("id")]
        public string Id { get; set; }

        [JsonProperty("name")]
        public string Name { get; set; }

        [JsonProperty("value")]
        public long Value { get; set; }
    }
}


