﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace ECOM.AWS.ServiceOrderApi.Entities
{
    public class StatusResponse
    {
        [JsonProperty("Code")]
        public long Code { get; set; }

        [JsonProperty("Message")]
        public object Message { get; set; }

        [JsonProperty("Result")]
        public Result Result { get; set; }
    }

    public class Result
    {
        [JsonProperty("OrderId")]
        public dynamic OrderId { get; set; }

        [JsonProperty("OrderStatus")]
        public string OrderStatus { get; set; }

        [JsonProperty("OrderReceiveStatus")]
        public string OrderReceiveStatus { get; set; }
    }
}
