﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ECOM.AWS.ServiceOrderApi.Core
{
    public static class StaticVariables
    {
        public static string Authorization { get; set; }
        public static string OrderStatusApiToken { get; set; }
        public static string OrderStatusApiDomain { get; set; }
        public static string UrlOrderStatus { get; set; }

        public static void SetStageVariables(Dictionary<string, string> variables)
        {
            if (variables != null)
            {
                Authorization = variables["Authorization"];
                OrderStatusApiToken = variables["OrderStatusApiToken"];
                OrderStatusApiDomain = variables["OrderStatusApiDomain"];
                UrlOrderStatus = variables["UrlOrderStatus"];
            }
            else
            {
                throw new Exception("No se encontrarón variables de etapa configuradas.");
            }
        }
    }
}
