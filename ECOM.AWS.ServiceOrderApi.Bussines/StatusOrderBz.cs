﻿using ECOM.AWS.ServiceOrderApi.Entities;
using ECOM.AWS.ServiceOrderApi.Services;
using System;
using System.Collections.Generic;
using System.Text;

namespace ECOM.AWS.ServiceOrderApi.Bussines
{
    public class StatusOrderBz
    {
        public StatusResponse GetOrderStatus(Dictionary<string, string> pathParams, Dictionary<string, string> StageVariables)
        {
            string infoError = string.Empty;
            const string kKeyWord = "norderid";
            string OrderId = pathParams[kKeyWord].Trim();
            infoError = "ECOM.AWS.ServiceOrderApi.Bussines.StatusOrderBz.GetOrderStatus()";
            StatusResponse orderStatus = new StatusResponse();
            try
            {
                ServiceOrder objebz = new ServiceOrder();
                orderStatus = objebz.GetServiceOrder(OrderId, StageVariables);
            }
            catch (Exception ex)
            {
                return new StatusResponse()
                {
                    Code = 637121961334425437,
                    Message = "Ocurrio un error en : "+ infoError+"\\"+"MoreInfo : "+ ex.Message.ToString(),
                    Result=null                    
                };
            }

            return orderStatus;
        }
    }
}
