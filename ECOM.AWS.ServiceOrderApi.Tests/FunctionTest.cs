using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

using Xunit;
using Amazon.Lambda.Core;
using Amazon.Lambda.TestUtilities;
using Amazon.Lambda.APIGatewayEvents;

using ECOM.AWS.ServiceOrderApi;
using ECOM.AWS.ServiceOrderApi.Main.OrderStatus;

namespace ECOM.AWS.ServiceOrderApi.Tests
{
    public class FunctionTest
    {
        Dictionary<string, string> stageVariables = null;
        public FunctionTest()
        {
            stageVariables = new Dictionary<string, string>() {
                { "Authorization", "vtexappkey-elektraqa-QOUWAD" },
                { "OrderStatusApiToken", "AAEDVKSCLNKGFOJXKYJXIRTOADUALSMMBYDKTLVJEXZHFELEITDFTAHJGQCSPRRSAXEGKZDZCZSLSMIVRQRROHTARUEDIDUMGGGHZFTJPECTIVRTHHAZAZETDDOQUARZ" },
                { "OrderStatusApiDomain", "https://elektraqa.vtexcommercestable.com" },
                {"UrlOrderStatus","https://elektraqa.vtexcommercestable.com.br/api/oms/pvt/orders/" }
            };

        }

        [Fact]
        public void TetGetMethod()
        {
            var context = new TestLambdaContext();
            APIGatewayProxyRequest request;
            APIGatewayProxyResponse response;

            //O-8000511,O-1000000000007
            var algo = new APIGatewayProxyRequest()
            {
                PathParameters = new Dictionary<string, string>() { { "norderid", "O-8000511" } },
                StageVariables = this.stageVariables
            };


            Function functions = new Function();
            var objeResult=functions.GetStatusOrder(algo, context);
        }
    }
}
